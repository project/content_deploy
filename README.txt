CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration and Usage
 * Support
 * Maintainers

INTRODUCTION
------------

The content deploy module is exetended version of Content Synchronization module (https://www.drupal.org/project/content_sync). This module provides a mechanism to export single content items as well as multiple content items from one environment to another including dependencies. This module also provides useful drush commands to export/import content.
This module has extra features of single entity import/export and also we can schedule auto deployment on specific time as well.

REQUIREMENTS
------------

1. Drush with version >= 9
2. Serialization : This module is Drupal core's part.

INSTALLATION
------------

1. Create directory on Drupal root : content/sync
Here "sync" is the sub-folder of "content" directory.

2. Give permission to this "content" directory recursively :

$ chmod -R 755 content/

3. Add this code in settings.php file :

global $content_directories;
$content_directories['sync'] = 'content/sync';

This will define the path where the exported content will be saved.

4. Extract module at drupal/modules/contrib directory or get it with composer require command and enable it from browser
by going in this path /admin/modules.


CONFIGURATION AND USAGE
-----------------------

Configure at admin/config/development/content

###1. To do full content export from terminal by using Drush :

> Go to site root from terminal.

> Run this command :

$ drush content-deploy:export

> This process will take time. It will export all content and place it in directory : content/sync (This directory path is given in settings.php file)

> Push the directory content/sync to the destination site where you want to import content.

> If you want to do export for particular entity types, you can use this command :

$ drush content-deploy:export --entity-types=node,user,block,file,media,taxonomy,paragraph

Here node,user,block etc are entity types.


> To import the full conent by Drush. Pull the latest updated content/sync directory files from SOURCE site.

> Go to site root from terminal.

> Run this command :

$ drush content-deploy:import

> This process will take time. It will import all content from directory : content/sync

> If you want to do import for particular entity types, you can use this command :

$ drush content-deploy:import --entity-types=node,user,block,file,media,taxonomy,paragraph

Here node,user,block etc are entity types.


###2. To Sync the content : /admin/config/development/content

###3. To do Full content Archive Export : /admin/config/development/content/export/full

###4. To do Full content Archive Import : /admin/config/development/content/import/full

###5. To Export single entity without dependencies : /admin/config/development/content/export/single

###6. To Import single entity without dependencies : /admin/config/development/content/import/single

###7. To Export single node with dependencies : /node/{nid}/content-sync-export

###8. To Import single node with dependencies : /admin/config/development/content/import/single-with-dependencies

###9. To bulk export nodes use the action "Export Nodes" from here : /admin/content

###10. To bulk import nodes : /admin/config/development/content/import/bulk-with-dependencies

###11. To schedule deployment : 
Firstly update the environment settings from here > /admin/config/development/content/environment-settings
And then you can schedule the deployment from here > /admin/config/development/content/deploy

###12. To check/update/cancel the scheduled deployments : /admin/config/development/content/schedule-deploys-logs


SUPPORT
-------

This open source project is supported by the Drupal.org community. To report a
bug, request a feature, or upgrade to the latest version, please visit the
project page: http://drupal.org/project/content_deploy


MAINTAINERS
-----------

Jagraj Singh Gill (jagraj)
https://www.drupal.org/u/jagraj

Shikha Dawar (Shikha_LNweb) 
https://www.drupal.org/u/shikha_lnweb

Pankaj Kumar (Pankaj_LNweb)
https://www.drupal.org/u/pankaj_lnweb


