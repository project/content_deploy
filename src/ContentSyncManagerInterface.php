<?php

namespace Drupal\content_deploy;


/**
 * Interface ContentSyncManagerInterface.
 *
 * @package Drupal\content_deploy
 */
interface ContentSyncManagerInterface {

  /**
   * @return \Drupal\content_deploy\Importer\ContentImporterInterface
   */
  public function getContentImporter();

  /**
   * @return \Drupal\content_deploy\Exporter\ContentExporterInterface
   */
  public function getContentExporter();

  /**
   * @return \Symfony\Component\Serializer\Serializer
   */
  public function getSerializer();

  /**
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public function getEntityTypeManager();

}
