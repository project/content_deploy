<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class ContentEnvSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_env_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('content_deploy.settings');

    $form['curr_auto_deploy_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto Deploy directory for current site'),
      '#description' => $this->t('This is the directory path for auto-deployment on current site. Please use the server path for directory. Example : /my/auto-deploy/dir/path'),
      '#default_value' => $config->get('content_deploy.curr_auto_deploy_dir'),
      '#required' => TRUE,
    ];

    $form['dev_env'] = [
      '#type' => 'fieldset',
      '#title' => 'Dev Environment',
    ];

    $form['dev_env']['dev_env_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dev Environment URL'),
      '#description' => $this->t('This is the url of the dev site and if anything given in this field then below SSH details will be considered required. Example : https://example.com'),
      '#default_value' => $config->get('content_deploy.dev_env_url'),
    ];

    $form['dev_env']['ssh_details'] = [
      '#type' => 'details',
      '#title' => t('SSH Details'),
    ];

    $form['dev_env']['ssh_details']['dev_ssh_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Name'),
      '#description' => $this->t('This is the SSH user name of the dev site.'),
      '#default_value' => $config->get('content_deploy.dev_ssh_username'),
      '#states' => [
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_ssh_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('This is the SSH host of the dev site.'),
      '#default_value' => $config->get('content_deploy.dev_ssh_host'),
      '#states' => [
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_authentication_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Type'),
      '#description' => $this->t('This is the method with which SSH connection will be authenticated.'),
      '#default_value' => $config->get('content_deploy.dev_authentication_type'),
      '#options' => [
        'password' => $this->t('Password'),
        'key' => $this->t('Key'),
      ],
      '#states' => [
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_ssh_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('This is the SSH password of the dev site.'),
      '#default_value' => base64_decode($config->get('content_deploy.dev_ssh_password')),
      '#states' => [
        'visible' => [
          ':input[name="dev_authentication_type"]' => ['value' => 'password'],
        ],
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
          ':input[name="dev_authentication_type"]' => ['value' => 'password'],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_ssh_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key Path'),
      '#description' => $this->t('Add the SSH public key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa.pub'),
      '#default_value' => $config->get('content_deploy.dev_ssh_public_key'),
      '#states' => [
        'visible' => [
          ':input[name="dev_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
          ':input[name="dev_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_ssh_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key Path'),
      '#description' => $this->t('Add the SSH private key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa'),
      '#default_value' => $config->get('content_deploy.dev_ssh_private_key'),
      '#states' => [
        'visible' => [
          ':input[name="dev_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
          ':input[name="dev_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['dev_env']['ssh_details']['dev_ssh_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#description' => $this->t('Port to maintain SSH connection. If left empty, default port 22 will be considered.'),
      '#default_value' => $config->get('content_deploy.dev_ssh_port'),
    ];

    $form['dev_env']['ssh_details']['dev_auto_deploy_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto-deploy directory path'),
      '#description' => $this->t('This is the directory path for auto-deployment on dev site. Please use the server path for directory. Example : /my/auto-deploy/dir/path'),
      '#default_value' => $config->get('content_deploy.dev_auto_deploy_dir'),
      '#states' => [
        'required' => [
          ':input[name="dev_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['stage_env'] = [
      '#type' => 'fieldset',
      '#title' => 'Stage Environment',
    ];

    $form['stage_env']['stage_env_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stage Environment URL'),
      '#description' => $this->t('This is the url of the Stage site and if anything given in this field then below SSH details will be considered required. Example : https://example.com'),
      '#default_value' => $config->get('content_deploy.stage_env_url'),
    ];

    $form['stage_env']['ssh_details'] = [
      '#type' => 'details',
      '#title' => t('SSH Details'),
    ];

    $form['stage_env']['ssh_details']['stage_ssh_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Name'),
      '#description' => $this->t('This is the SSH user name of the stage site.'),
      '#default_value' => $config->get('content_deploy.stage_ssh_username'),
      '#states' => [
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_ssh_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('This is the SSH host of the stage site.'),
      '#default_value' => $config->get('content_deploy.stage_ssh_host'),
      '#states' => [
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_authentication_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Type'),
      '#description' => $this->t('This is the method with which SSH connection will be authenticated.'),
      '#default_value' => $config->get('content_deploy.stage_authentication_type'),
      '#options' => [
        'password' => $this->t('Password'),
        'key' => $this->t('Key'),
      ],
      '#states' => [
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_ssh_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('This is the SSH password of the stage site.'),
      '#default_value' => base64_decode($config->get('content_deploy.stage_ssh_password')),
      '#states' => [
        'visible' => [
          ':input[name="stage_authentication_type"]' => ['value' => 'password'],
        ],
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
          ':input[name="stage_authentication_type"]' => ['value' => 'password'],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_ssh_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key Path'),
      '#description' => $this->t('Add the SSH public key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa.pub'),
      '#default_value' => $config->get('content_deploy.stage_ssh_public_key'),
      '#states' => [
        'visible' => [
          ':input[name="stage_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
          ':input[name="stage_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_ssh_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key Path'),
      '#description' => $this->t('Add the SSH private key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa'),
      '#default_value' => $config->get('content_deploy.stage_ssh_private_key'),
      '#states' => [
        'visible' => [
          ':input[name="stage_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
          ':input[name="stage_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['stage_env']['ssh_details']['stage_ssh_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#description' => $this->t('Port to maintain SSH connection. If left empty, default port 22 will be considered.'),
      '#default_value' => $config->get('content_deploy.stage_ssh_port'),
    ];

    $form['stage_env']['ssh_details']['stage_auto_deploy_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto-deploy directory path'),
      '#description' => $this->t('This is the directory path for auto-deployment on stage site. Please use the server path for directory. Example : /my/auto-deploy/dir/path'),
      '#default_value' => $config->get('content_deploy.stage_auto_deploy_dir'),
      '#states' => [
        'required' => [
          ':input[name="stage_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['prod_env'] = [
      '#type' => 'fieldset',
      '#title' => 'Production Environment',
    ];

    $form['prod_env']['prod_env_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production Environment URL'),
      '#description' => $this->t('This is the url of the Production site and if anything given in this field then below SSH details will be considered required. Example : https://example.com'),
      '#default_value' => $config->get('content_deploy.prod_env_url'),
    ];

    $form['prod_env']['ssh_details'] = [
      '#type' => 'details',
      '#title' => t('SSH Details'),
    ];

    $form['prod_env']['ssh_details']['prod_ssh_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Name'),
      '#description' => $this->t('This is the SSH user name of the prod site.'),
      '#default_value' => $config->get('content_deploy.prod_ssh_username'),
      '#states' => [
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_ssh_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('This is the SSH host of the prod site.'),
      '#default_value' => $config->get('content_deploy.prod_ssh_host'),
      '#states' => [
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_authentication_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Type'),
      '#default_value' => $config->get('content_deploy.prod_authentication_type'),
      '#description' => $this->t('This is the method with which SSH connection will be authenticated.'),
      '#options' => [
        'password' => $this->t('Password'),
        'key' => $this->t('Key'),
      ],
      '#states' => [
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_ssh_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('This is the SSH password of the prod site.'),
      '#default_value' => base64_decode($config->get('content_deploy.prod_ssh_password')),
      '#states' => [
        'visible' => [
          ':input[name="prod_authentication_type"]' => ['value' => 'password'],
        ],
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
          ':input[name="prod_authentication_type"]' => ['value' => 'password'],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_ssh_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key Path'),
      '#description' => $this->t('Add the SSH public key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa.pub'),
      '#default_value' => $config->get('content_deploy.prod_ssh_public_key'),
      '#states' => [
        'visible' => [
          ':input[name="prod_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
          ':input[name="prod_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_ssh_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key Path'),
      '#description' => $this->t('Add the SSH private key path here. Please use the server path here and this path should be of Current server. Example : /mydir/.ssh/id_rsa'),
      '#default_value' => $config->get('content_deploy.prod_ssh_private_key'),
      '#states' => [
        'visible' => [
          ':input[name="prod_authentication_type"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
          ':input[name="prod_authentication_type"]' => ['value' => 'key'],
        ],
      ],
    ];

    $form['prod_env']['ssh_details']['prod_ssh_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#description' => $this->t('Port to maintain SSH connection. If left empty, default port 22 will be considered.'),
      '#default_value' => $config->get('content_deploy.prod_ssh_port'),
    ];

    $form['prod_env']['ssh_details']['prod_auto_deploy_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto-deploy directory path'),
      '#description' => $this->t('This is the directory path for auto-deployment on prod site. Please use the server path for directory. Example : /my/auto-deploy/dir/path'),
      '#default_value' => $config->get('content_deploy.prod_auto_deploy_dir'),
      '#states' => [
        'required' => [
          ':input[name="prod_env_url"]' => ['!value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @return void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Check if dir exist and writable.
    if (empty($form_state->getValue('curr_auto_deploy_dir')) || !is_dir($form_state->getValue('curr_auto_deploy_dir'))) {
      $form_state->setErrorByName('curr_auto_deploy_dir', t('Directory doesn\'t exist or undefined.'));
      return;
    }
    elseif (!is_writable($form_state->getValue('curr_auto_deploy_dir'))) {
      $form_state->setErrorByName('curr_auto_deploy_dir', t('Directory is not writable, Please update the permissions of directory.'));
      return;
    }

    // Check if connection details given.
    $devConnection = FALSE;
    $stageConnection = FALSE;
    $prodConnection = FALSE;

    if (!empty($form_state->getValue('dev_env_url'))) {
      $devConnection = TRUE;
    }
    if (!empty($form_state->getValue('stage_env_url'))) {
      $stageConnection = TRUE;
    }
    if (!empty($form_state->getValue('prod_env_url'))) {
      $prodConnection = TRUE;
    }

    // Check if ssh2_connect function doesn't exist and any env is using password authentication because pass auth works with ssh2_connect only.
    if (!function_exists("ssh2_connect")) {
      $errorMsg = 'SSH2 PECL extension is not installed/enabled in PHP to maintain the connection using SSH password. Please install or enable it.';
      if ($devConnection && $form_state->getValue('dev_authentication_type') == 'password') {
        $form_state->setErrorByName('dev_authentication_type', t($errorMsg));
        return;
      }
      if ($stageConnection && $form_state->getValue('stage_authentication_type') == 'password') {
        $form_state->setErrorByName('stage_authentication_type', t($errorMsg));
        return;
      }
      if ($prodConnection && $form_state->getValue('prod_authentication_type') == 'password') {
        $form_state->setErrorByName('prod_authentication_type', t($errorMsg));
        return;
      }
    }

    // Check Dev SSH details and connection.
    if ($devConnection) {
      $formValues = $form_state->cleanValues()->getValues();
      $response = $this->_validate_ssh_connection($formValues, 'dev');
      if ($response['status'] == 0) {
        $form_state->setErrorByName($response['fieldName'], t($response['errorMessage']));
      }
    }

    // Check Stage SSH details and connection.
    if ($stageConnection) {
      $formValues = $form_state->cleanValues()->getValues();
      $response = $this->_validate_ssh_connection($formValues, 'stage');
      if ($response['status'] == 0) {
        $form_state->setErrorByName($response['fieldName'], t($response['errorMessage']));
      }
    }

    // Check Prod SSH details and connection.
    if ($prodConnection) {
      $formValues = $form_state->cleanValues()->getValues();
      $response = $this->_validate_ssh_connection($formValues, 'prod');
      if ($response['status'] == 0) {
        $form_state->setErrorByName($response['fieldName'], t($response['errorMessage']));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('content_deploy.settings');

    // Current.
    $config->set('content_deploy.curr_auto_deploy_dir', $form_state->getValue('curr_auto_deploy_dir'));

    // Dev env.
    $config->set('content_deploy.dev_env_url', $form_state->getValue('dev_env_url'));
    $config->set('content_deploy.dev_ssh_username', $form_state->getValue('dev_ssh_username'));
    $config->set('content_deploy.dev_ssh_host', $form_state->getValue('dev_ssh_host'));
    $config->set('content_deploy.dev_authentication_type', $form_state->getValue('dev_authentication_type'));
    $config->set('content_deploy.dev_ssh_password', base64_encode($form_state->getValue('dev_ssh_password')));
    $config->set('content_deploy.dev_ssh_public_key', $form_state->getValue('dev_ssh_public_key'));
    $config->set('content_deploy.dev_ssh_private_key', $form_state->getValue('dev_ssh_private_key'));
    $config->set('content_deploy.dev_ssh_port', $form_state->getValue('dev_ssh_port'));
    $config->set('content_deploy.dev_auto_deploy_dir', $form_state->getValue('dev_auto_deploy_dir'));

    // Stage env.
    $config->set('content_deploy.stage_env_url', $form_state->getValue('stage_env_url'));
    $config->set('content_deploy.stage_ssh_username', $form_state->getValue('stage_ssh_username'));
    $config->set('content_deploy.stage_ssh_host', $form_state->getValue('stage_ssh_host'));
    $config->set('content_deploy.stage_authentication_type', $form_state->getValue('stage_authentication_type'));
    $config->set('content_deploy.stage_ssh_password', base64_encode($form_state->getValue('stage_ssh_password')));
    $config->set('content_deploy.stage_ssh_public_key', $form_state->getValue('stage_ssh_public_key'));
    $config->set('content_deploy.stage_ssh_private_key', $form_state->getValue('stage_ssh_private_key'));
    $config->set('content_deploy.stage_ssh_port', $form_state->getValue('stage_ssh_port'));
    $config->set('content_deploy.stage_auto_deploy_dir', $form_state->getValue('stage_auto_deploy_dir'));

    // Prod env.
    $config->set('content_deploy.prod_env_url', $form_state->getValue('prod_env_url'));
    $config->set('content_deploy.prod_ssh_username', $form_state->getValue('prod_ssh_username'));
    $config->set('content_deploy.prod_ssh_host', $form_state->getValue('prod_ssh_host'));
    $config->set('content_deploy.prod_authentication_type', $form_state->getValue('prod_authentication_type'));
    $config->set('content_deploy.prod_ssh_password', base64_encode($form_state->getValue('prod_ssh_password')));
    $config->set('content_deploy.prod_ssh_public_key', $form_state->getValue('prod_ssh_public_key'));
    $config->set('content_deploy.prod_ssh_private_key', $form_state->getValue('prod_ssh_private_key'));
    $config->set('content_deploy.prod_ssh_port', $form_state->getValue('prod_ssh_port'));
    $config->set('content_deploy.prod_auto_deploy_dir', $form_state->getValue('prod_auto_deploy_dir'));

    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_deploy.settings',
    ];
  }

  /**
   * Provides help to validate SSH connection.
   */
  private function _validate_ssh_connection($formValues, $env) {

    if (empty($formValues[$env . '_ssh_username'])) {
      return [
        'status' => 0,
        'fieldName' => $env . '_ssh_username',
        'errorMessage' => 'SSH user-name is undefined.',
      ];
    }
    if (empty($formValues[$env . '_ssh_host'])) {
      return [
        'status' => 0,
        'fieldName' => $env . '_ssh_host',
        'errorMessage' => 'SSH host is undefined.',
      ];
    }
    $userName = $formValues[$env . '_ssh_username'];
    $host = $formValues[$env . '_ssh_host'];

    $port = 22;
    if (!empty($formValues[$env . '_ssh_port'])) {
      $port = $formValues[$env . '_ssh_port'];
    }
    $remoteDir = $formValues[$env . '_auto_deploy_dir'];
    $remoteDir = rtrim($remoteDir, '/');
    if (empty($remoteDir)) {
      return [
        'status' => 0,
        'fieldName' => $env . '_auto_deploy_dir',
        'errorMessage' => 'Auto deploy directory is undefined.',
      ];
    }

    // Check SSH connection.
    if ($formValues[$env . '_authentication_type'] == 'key') {

      if (!file_exists($formValues[$env . '_ssh_public_key']) || empty($formValues[$env . '_ssh_public_key'])) {
        return [
          'status' => 0,
          'fieldName' => $env . '_ssh_public_key',
          'errorMessage' => 'Public key doesn\'t exist or undefined.',
        ];
      }
      elseif (!file_exists($formValues[$env . '_ssh_private_key']) || empty($formValues[$env . '_ssh_private_key'])) {
        return [
          'status' => 0,
          'fieldName' => $env . '_ssh_private_key',
          'errorMessage' => 'Private key doesn\'t exist or undefined.',
        ];
      }
      else {
        $prvKey = $formValues[$env . '_ssh_private_key'];
        $output = NULL;

        // To check if dir exist on target server.
        $subCmd = "test -d '" . $remoteDir . "' && echo true || echo false;";

        // To check server connection.
        $cmd = 'ssh -i "' . $prvKey . '" ' . $userName . '@' . $host . ' -p ' . $port . ' "' . $subCmd . '"';
        exec($cmd, $output);

        if (empty($output)) {
          return [
            'status' => 0,
            'fieldName' => $env . '_env',
            'errorMessage' => 'Unable to maintain SSH connection using authentication method key for ' . $env . ' environment.',
          ];
        }
        elseif (!isset($output[0]) || $output[0] == 'false') {
          return [
            'status' => 0,
            'fieldName' => $env . '_auto_deploy_dir',
            'errorMessage' => 'Given auto deploy dir doesn\'t exist on ' . $env . ' environment.',
          ];
        }
      }
    }
    elseif ($formValues[$env . '_authentication_type'] == 'password') {

      if (empty($formValues[$env . '_ssh_password'])) {
        return [
          'status' => 0,
          'fieldName' => $env . '_ssh_password',
          'errorMessage' => 'Password undefined.',
        ];
      }

      $pass = $formValues[$env . '_ssh_password'];
      $connection = @ssh2_connect($host, $port);
      if (!@ssh2_auth_password($connection, $userName, $pass)) {
        return [
          'status' => 0,
          'fieldName' => $env . '_env',
          'errorMessage' => 'Unable to maintain SSH connection using authentication method password for ' . $env . ' environment.',
        ];
      }
      if (!@ssh2_sftp($connection)) {
        return [
          'status' => 0,
          'fieldName' => $env . '_env',
          'errorMessage' => 'Unable to maintain SFTP connection using authentication method password for ' . $env . ' environment.',
        ];
      }

      // Check if dir exist on target server.
      $cmd = "test -d '" . $remoteDir . "' && echo true || echo false;";
      $stream = ssh2_exec($connection, $cmd);
      stream_set_blocking($stream, TRUE);
      $output = stream_get_contents($stream);

      if (empty($output) || $output == 'false') {
        return [
          'status' => 0,
          'fieldName' => $env . '_auto_deploy_dir',
          'errorMessage' => 'Given auto deploy dir doesn\'t exist on ' . $env . ' environment.',
        ];
      }

      $connection = NULL;
      unset($connection);
    }
    else {
      return [
        'status' => 0,
        'fieldName' => $env . '_authentication_type',
        'errorMessage' => 'Authentication method is undefined.',
      ];
    }

    return [
      'status' => 1,
    ];
  }

}
