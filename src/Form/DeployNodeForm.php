<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides the database logging filter form.
 */
class DeployNodeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deploy_node_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $currTimestamp = \Drupal::time()->getCurrentTime();
    $entity = \Drupal::routeMatch()->getParameter('node');
    $db = \Drupal::database();
    $query = $db->select('cd_auto_nodes_export', 'ane');
    $query->condition('ane.node_uuid', $entity->uuid());
    $query->condition('ane.deployment_status', 0);
    $query->condition('deployment_time', $currTimestamp, '>');
    $query->fields('ane');
    $schedule_node = $query->execute()->fetchAll();

    $form['action'] = [
      '#type' => 'details',
      '#title' => $this->t('Action'),
      '#open' => TRUE,
    ];

    // Get actual user role.
    $current_user_roles = \Drupal::currentUser()->getRoles();
    // Check if user is not admin.
    if (in_array('administrator', $current_user_roles)) {
      $options = ['dev' => 'Dev', 'stage' => 'Stage', 'prod' => 'Prod'];
    }
    else {
      $options = ['prod' => 'Prod'];
    }
    $form['action']['environment'] = [
      '#title' => $this->t('Target Environment'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => $options,
      '#attributes' => ['class' => ['inline']],
      '#default_value' => isset($schedule_node['0']->target_environment) ? $schedule_node['0']->target_environment : 'prod',
    ];

    $form['action']['status'] = [
      '#title' => $this->t('Deployed Node Status'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => ['1' => t('Published'), '0' => t('Unpublished')],
      '#default_value' => isset($schedule_node['0']->node_target_status) ? $schedule_node['0']->node_target_status : '1',
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['action']['deploy_date_time'] = [
      '#title' => $this->t('Deployment Time'),
      '#type' => 'datetime',
      '#required' => TRUE,
      '#default_value' => isset($schedule_node['0']->deployment_time) ? DrupalDateTime::createFromTimestamp($schedule_node['0']->deployment_time) : NULL,
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Deployment'),
    ];

    if (!empty($schedule_node)) {
      $form['filters']['actions']['submit']['#value'] = $this->t('Update Deployment');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('environment')) || $form_state->getValue('environment') == 'none') {
      $form_state->setErrorByName('environment', t('No Environment selected or You don\'t have access to deploy on any Environment.'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = \Drupal::routeMatch()->getParameter('node');
    $nodeTargetStatus = $form_state->getValue('status');
    $targetEnv = $form_state->getValue('environment');
    $deployDateTime = $form_state->getValue('deploy_date_time');
    $deployDateTime = $deployDateTime->getTimestamp();

    $current_user = User::load(\Drupal::currentUser()->id());
    $currentUserUUID = $current_user->uuid();
    $currTimestamp = \Drupal::time()->getCurrentTime();

    $db = \Drupal::database();
    $query = $db->select('cd_auto_nodes_export', 'ane');
    $query->condition('ane.node_uuid', $entity->uuid());
    $query->fields('ane');
    $autoDeployQueryResults = $query->execute()->fetchAll();
    if ($autoDeployQueryResults) {
      $db->update('cd_auto_nodes_export')
        ->fields([
          'node_uuid' => $entity->uuid(),
          'node_target_status' => $nodeTargetStatus,
          'target_environment' => $targetEnv,
          'deployment_time' => $deployDateTime,
          'user_uuid' => $currentUserUUID,
          'created' => $currTimestamp,
        ])
        ->condition('node_uuid', $entity->uuid())
        ->condition('deployment_status', 0)
        ->condition('deployment_time', $currTimestamp, '>')
        ->execute();
      $action = 'update';
    }
    else {
      $db->insert('cd_auto_nodes_export')
        ->fields(['node_uuid', 'node_target_status', 'target_environment', 'deployment_time', 'user_uuid', 'created'])
        ->values([$entity->uuid(), $nodeTargetStatus, $targetEnv, $deployDateTime, $currentUserUUID, $currTimestamp])
        ->execute();
      $action = 'added';
    }

    return \Drupal::messenger()->addMessage(t('This node has been :action in list of auto deployment.', [':action' => $action]), 'status');

  }

}
