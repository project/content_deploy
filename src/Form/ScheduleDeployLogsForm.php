<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\content_deploy\Utility\AutoContentDeployHelper;

/**
 * Gives the ability to view/update/delete Schedule deployments.
 */
class ScheduleDeployLogsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'schedule_deploy_logs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $envOptions = ['dev' => 'Dev', 'stage' => 'Stage', 'prod' => 'Prod'];
    $enabledEnvOptions = [];
    $enabledEnvOptions['default'] = $this->t('Default');
    $defaultEnvValue = 'default';
    foreach ($envOptions as $envValue => $env) {
      $envDetails = \Drupal::config('content_deploy.settings')->get('content_deploy.' . $envValue . '_env_url');
      if (\Drupal::currentUser()->hasPermission($envValue . ' deploy content') && !empty($envDetails)) {
        $enabledEnvOptions[$envValue] = $env;
      }
    }

    if (empty($enabledEnvOptions)) {
      $enabledEnvOptions['prod'] = 'Prod';
    }
    elseif (!array_key_exists($defaultEnvValue, $enabledEnvOptions)) {
      $defaultEnvValue = array_key_first($enabledEnvOptions);
    }

    $form['environment'] = [
      '#title' => $this->t('Target Environment'),
      '#type' => 'radios',
      '#options' => $enabledEnvOptions,
      '#default_value' => $defaultEnvValue,
      '#attributes' => ['class' => ['inline']],
    ];

    $form['status'] = [
      '#title' => $this->t('Deployed Node Status'),
      '#type' => 'radios',
      '#options' => ['default' => t('Default'), '1' => t('Published'), '0' => t('Unpublished')],
      '#default_value' => 'default',
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['deploy_date_time'] = [
      '#title' => $this->t('Deployment Time'),
      '#type' => 'datetime',
    ];

    $form['action'] = [
      '#title' => $this->t('Action for deployments'),
      '#type' => 'select',
      '#options' => [
        'delete' => $this->t('Delete deployments'),
        'update' => $this->t('Update deployments'),
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected items'),
    ];

    $header = [
      'node_title' => [
        'data' => t('Node Title'),
        'specifier' => 'node_title',
      ],
      'target_environment' => [
        'data' => t('Target Environment'),
        'specifier' => 'target_environment',
      ],
      'deployment_time' => [
        'data' => t('Deployment Time'),
        'specifier' => 'deployment_time',
      ],
      'node_target_status' => [
        'data' => t('Target Node Status'),
        'specifier' => 'node_target_status',
      ],
      'created_time' => [
        'data' => t('Created Time'),
        'specifier' => 'created_time',
      ],
      'deployment_status' => [
        'data' => t('Deployment Status'),
        'specifier' => 'deployment_status',
      ],
      'actions' => [
        'data' => t('Actions'),
        'specifier' => 'actions',
      ],
    ];

    $form['deployment_nodes'] = [
      '#type' => 'tableselect',
      '#options'  => [],
      '#header' => $header,
      '#empty' => $this->t('No deployments data found.'),
      '#attributes' => ['class' => ['views-table views-view-table cols-8 responsive-enabled sticky-enabled sticky-table']],
    ];

    $currTimestamp = \Drupal::time()->getCurrentTime();
    $connection = Database::getConnection();
    $query = $connection->select('cd_auto_nodes_export', 'ane');
    $query->join('node', 'n', 'n.uuid = ane.node_uuid');
    $query->join('node_field_data', 'nfd', 'nfd.nid = n.nid');
    $query->fields('ane');
    $query->fields('n', ['nid']);
    $query->fields('nfd', ['title']);
    $query->orderBy('ane.created', 'DESC');
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(50);
    $autoDeployQueryResults = $pager->execute()->fetchAll();

    foreach ($autoDeployQueryResults as $key => $deploymentNode) {
      $row = [];
      $row['node_title'] = ['data' => $deploymentNode->title];
      $row['target_environment'] = ['data' => ucfirst($deploymentNode->target_environment)];
      $row['deployment_time'] = ['data' => date('m/d/Y H:i', $deploymentNode->deployment_time)];

      $nodeTargetStatus = t('Publish');
      if ($deploymentNode->node_target_status == 0) {
        $nodeTargetStatus = t('Unpublish');
      }
      $row['node_target_status'] = ['data' => $nodeTargetStatus];

      $row['created_time'] = ['data' => date('m/d/Y H:i', $deploymentNode->created)];

      $deploymentStatus = t('Deployed');
      if ($deploymentNode->deployment_status == 0) {
        $deploymentStatus = t('Not deployed');
      }
      $row['deployment_status'] = ['data' => $deploymentStatus];

      $actions = '-';
      if ($deploymentNode->deployment_time > $currTimestamp) {

        $delete = Url::fromRoute('content_deploy.delete_auto_deployment', [
          'target_env' => $deploymentNode->target_environment,
          'node_uuid' => $deploymentNode->node_uuid,
          'deployment_time' => $deploymentNode->deployment_time,
          'created' => $deploymentNode->created,
        ])->toString();

        $edit = Url::fromRoute('content.content_deploy', [
          'node' => $deploymentNode->nid,
        ])->toString();

        $actions = Markup::create("<a href='" . $edit . "'>Edit</a>&nbsp;&nbsp;<a href='" . $delete . "'>Delete</a>");
      }
      $row['actions'] = ['data' => $actions];

      $form['deployment_nodes']['#options'][$deploymentNode->node_uuid] = $row;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty($form_state->getValue('action'))) {
      $form_state->setErrorByName('action', t('Action for deployment is required.'));
      return;
    }

    $tableNodes = $form_state->getValue('deployment_nodes');
    $selectedNodes = array_filter($tableNodes, function ($singleTableNode) {
      return $singleTableNode;
    });
    if (empty($selectedNodes)) {
      $form_state->setErrorByName('deployment_nodes', t('No deployments selected. Please select the deployments.'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $tableNodes = $form_state->getValue('deployment_nodes');
    $selectedNodes = array_filter($tableNodes, function ($singleTableNode) {
      return $singleTableNode;
    });

    if (!empty($selectedNodes)) {
      if ($form_state->getValue('action') == 'delete') {
        $count = count($selectedNodes);
        AutoContentDeployHelper::deleteDeployments($selectedNodes);
        return \Drupal::messenger()->addMessage(t('Selected ' . $count . ' deployments has been deleted successfully.'), 'status');
      }
      elseif ($form_state->getValue('action') == 'update') {
        $count = count($selectedNodes);

        $targetEnv = NULL;
        if (!empty($form_state->getValue('environment')) && $form_state->getValue('environment') !== 'default') {
          $targetEnv = $form_state->getValue('environment');
        }

        $targetNodeStatus = NULL;
        if ($form_state->getValue('status') !== NULL && $form_state->getValue('status') !== 'default') {
          $targetNodeStatus = $form_state->getValue('status');
        }

        $deploymentTime = NULL;
        if (!empty($form_state->getValue('deploy_date_time'))) {
          $deploymentTime = $form_state->getValue('deploy_date_time');
          $deploymentTime = $deploymentTime->getTimestamp();
        }

        AutoContentDeployHelper::updateDeployments($selectedNodes, $targetEnv, $targetNodeStatus, $deploymentTime);
        return \Drupal::messenger()->addMessage(t('Selected ' . $count . ' deployments has been updated successfully.'), 'status');
      }
    }
  }

}
