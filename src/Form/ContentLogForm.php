<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Provides the database logging filter form.
 */
class ContentLogForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $header=[], $rows=[]) {

    $form['log_table'] = [
      '#type' => 'tableselect',
      '#options'  => $rows,
      '#header' => $header,
      '#empty' => $this->t('Data not found'),
      '#attributes' => ['id' => 'admin-cslog', 'class' => ['admin-cslog']],
    ];

    $form['log_pager'] = [
      '#type' => 'pager',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {


  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
