<?php

namespace Drupal\content_deploy\Form;

use Drupal\content_deploy\Exporter\ContentExporterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Archiver\ArchiveTar;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\content_deploy\Utility\ContentDeployNodeExportLogsHelper;

/**
 * Provides a form for exporting a single content file.
 */
class ContentSyncExportForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityBundleManager;

  /**
   * @var \Drupal\content_deploy\Exporter\ContentExporterInterface
   */
  protected $contentExporter;

  /**
   * @var
   */
  protected $exportedFiles;

  /**
   * Constructs a new ContentSingleExportForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_bundle_manager
   *
   * @param \Drupal\content_deploy\Exporter\ContentExporterInterface $content_exporter
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfo $entity_bundle_manager, ContentExporterInterface $content_exporter, $exportedFiles = []) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleManager = $entity_bundle_manager;
    $this->contentExporter = $content_exporter;
    $this->exportedFiles = $exportedFiles;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('content_deploy.exporter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_single_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $content_type = 'node', $content_name = NULL, $node = NULL) {
    $form['export'] = [
      '#title' => $this->t('Here is your configuration:'),
      '#type' => 'textarea',
      '#rows' => 24,
      '#attributes' => ['readonly' => TRUE],
      '#prefix' => '<div id="edit-export-wrapper">',
      '#suffix' => '</div>',
    ];

    $entity = $node;
    $entity_type = 'node';
    // Generate the YAML file.
    $serializer_context = [];
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
    }
    $exported_entity = $this->contentExporter->exportEntity($entity, $serializer_context);

    // Create the name.
    $name = $entity_type . "." . $entity->bundle() . "." . $entity->uuid();

    /* get the node dependencies */
    $entityDependencies = [];
    if (!empty($exported_entity)) {
      $decodedEntity = Yaml::decode($exported_entity);
      if (isset($decodedEntity['_content_deploy']['entity_dependencies']) && !empty($decodedEntity['_content_deploy']['entity_dependencies'])) {
        $entityDependencies = $decodedEntity['_content_deploy']['entity_dependencies'];
      }
    }

    // Return form values.
    $form['export']['#value'] = $exported_entity;
    $form['export']['#description'] = $this->t('Filename: %name', ['%name' => $name . '.yml']);
    $form['file_name'] = [
      '#type' => 'hidden',
      '#default_value' => $name,
    ];

    $form['entity_dependencies'] = [
      '#type' => 'value',
      '#value' => $entityDependencies,
    ];

    $form['org_exported_entity'] = [
      '#type' => 'value',
      '#value' => $entity,
    ];

    $form['is_unpublish'] = [
      '#type' => 'checkbox',
      '#title' => t('Export node as unpublished'),
    ];
    $form['is_unpublish_dep'] = [
      '#type' => 'checkbox',
      '#title' => t('Export dependencies as unpublished'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export with Dependencies'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $file_system = \Drupal::service('file_system');
    $nodeFileName = $form_state->getValue('file_name');
    $nodeFileContent = $form_state->getValue('export');
    $orgExportedEntity = $form_state->getValue('org_exported_entity');
    $defaultOwnerUid = 0;
    $defaultOwnerUUID = 0;
    if ($orgExportedEntity->getOwnerId()) {
      $defaultOwnerUid = $orgExportedEntity->getOwnerId();
      $defaultOwnerUUID = $orgExportedEntity->getOwner()->uuid();
    }
    //Exported as unpublished
    $isUnpublish = $form_state->getValue('is_unpublish');
    $isUnpublishDep = $form_state->getValue('is_unpublish_dep');
    if ($isUnpublish) {
      $orgExportedEntity->setUnpublished();
      $nodeFileContent = $this->contentExporter->exportEntity($orgExportedEntity, []);
    }
    $archiveFilepath = \Drupal::service('file_system')->getTempDirectory() . '/' . $nodeFileName . '.tar.gz';
    if (file_exists($archiveFilepath)) {
      $file_system->delete($archiveFilepath);
    }
    $archiver = new ArchiveTar($archiveFilepath, 'gz');

    /* get the node dependencies */
    if (!empty($form_state->getValue('entity_dependencies'))) {
      $entityDependencies = $form_state->getValue('entity_dependencies');
      foreach ($entityDependencies as $entityType => $singleTypeEntityDependency) {
        foreach ($singleTypeEntityDependency as $singleEntityDependency) {
          $attachedEntityDetail = explode('.', $singleEntityDependency);
          $attachedEntityUUID = end($attachedEntityDetail);
          if (!empty($attachedEntityUUID)) {
            if (in_array($entityType . '.' . $attachedEntityUUID, $this->exportedFiles) || (strpos($nodeFileName, "$entityType.") !== FALSE && strpos($nodeFileName, ".$attachedEntityUUID") !== FALSE)) {
              continue;
            }
            $attachedEntity = \Drupal::service('entity.repository')->loadEntityByUuid($entityType, $attachedEntityUUID);
            if ($attachedEntity->hasField('uid') && empty($attachedEntity->get('uid')->value)) {
              $attachedEntity->set('uid', $defaultOwnerUid);
            }
            if (!empty($attachedEntity)) {
              $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
              if (!empty($language) && $attachedEntity->hasTranslation($language)) {
                $attachedEntity = $attachedEntity->getTranslation($language);
              }
              $attachedEntityBundle = $attachedEntity->bundle();
              $attachedEntityFileName = $entityType . "." . $attachedEntityBundle . "." . $attachedEntityUUID;
              if ($isUnpublishDep && $entityType == 'node') {
                $attachedEntity->setUnpublished();
              }
              $attachedExportedEntity = $this->contentExporter->exportEntity($attachedEntity, []);
              $attachedDecodedEntity = Yaml::decode($attachedExportedEntity);
              if ($entityType == 'file') {
                $uri = $attachedEntity->getFileUri();
                if (file_exists($uri)) {
                  $scheme = \Drupal::service('stream_wrapper_manager')->getScheme($uri);
                  $archiver->addModify([$uri], "$nodeFileName/attached_files/$scheme", "$scheme://");
                }
              }
              $archiver->addString("$nodeFileName/attached_entities/$entityType/$attachedEntityBundle/$attachedEntityFileName.yml", $attachedExportedEntity);
              $this->exportedFiles[] = $entityType . '.' . $attachedEntityUUID;
              if (isset($attachedDecodedEntity['_content_deploy']['entity_dependencies']) && !empty($attachedDecodedEntity['_content_deploy']['entity_dependencies'])) {
                /* Export entity_dependencies recursively. */
                $this->_export_entity_dependencies($attachedDecodedEntity['_content_deploy']['entity_dependencies'], $nodeFileName, $archiver, $defaultOwnerUid, $isUnpublishDep);
              }
            }
          }
        }
      }
    }

    /* get the parent node */
    $this->exportedFiles[] = $orgExportedEntity->getEntityTypeId() . '.' . $orgExportedEntity->uuid();
    $archiver->addString("$nodeFileName/parent/$nodeFileName.yml", $nodeFileContent);

    $response = new BinaryFileResponse($archiveFilepath);
    $response->setContentDisposition('attachment', $nodeFileName . '.tar.gz');
    $form_state->setResponse($response);
    ContentDeployNodeExportLogsHelper::createLog($orgExportedEntity->uuid(), $defaultOwnerUUID);
    return $response;
  }

  /**
   * Provides help to export child dependencies.
   */
  private function _export_entity_dependencies($entityDependencies, $nodeFileName, $archiver, $defaultOwnerUid, $isUnpublishDep) {
    $file_system = \Drupal::service('file_system');
    foreach ($entityDependencies as $entityType => $singleTypeEntityDependency) {
      foreach ($singleTypeEntityDependency as $singleEntityDependency) {
        $attachedEntityDetail = explode('.', $singleEntityDependency);
        $attachedEntityUUID = end($attachedEntityDetail);
        if (!empty($attachedEntityUUID)) {
          if (in_array($entityType . '.' . $attachedEntityUUID, $this->exportedFiles) || (strpos($nodeFileName, "$entityType.") !== FALSE && strpos($nodeFileName, ".$attachedEntityUUID") !== FALSE)) {
            continue;
          }
          $attachedEntity = \Drupal::service('entity.repository')->loadEntityByUuid($entityType, $attachedEntityUUID);
          if ($attachedEntity->hasField('uid') && empty($attachedEntity->get('uid')->value)) {
            $attachedEntity->set('uid', $defaultOwnerUid);
          }
          if (!empty($attachedEntity)) {
            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            if (!empty($language) && $attachedEntity->hasTranslation($language)) {
              $attachedEntity = $attachedEntity->getTranslation($language);
            }
            $attachedEntityBundle = $attachedEntity->bundle();
            $attachedEntityFileName = $entityType . "." . $attachedEntityBundle . "." . $attachedEntityUUID;
            if ($isUnpublishDep && $entityType == 'node') {
              $attachedEntity->setUnpublished();
            }
            $attachedExportedEntity = $this->contentExporter->exportEntity($attachedEntity, []);
            $attachedDecodedEntity = Yaml::decode($attachedExportedEntity);
            if ($entityType == 'file') {
              $uri = $attachedEntity->getFileUri();
              if (file_exists($uri)) {
                $scheme = \Drupal::service('stream_wrapper_manager')->getScheme($uri);
                $archiver->addModify([$uri], "$nodeFileName/attached_files/$scheme", "$scheme://");
              }
            }
            $archiver->addString("$nodeFileName/attached_entities/$entityType/$attachedEntityBundle/$attachedEntityFileName.yml", $attachedExportedEntity);
            $this->exportedFiles[] = $entityType . '.' . $attachedEntityUUID;
            if (isset($attachedDecodedEntity['_content_deploy']['entity_dependencies']) && !empty($attachedDecodedEntity['_content_deploy']['entity_dependencies'])) {
              /* Export entity_dependencies recursively. */
              $this->_export_entity_dependencies($attachedDecodedEntity['_content_deploy']['entity_dependencies'], $nodeFileName, $archiver, $defaultOwnerUid, $isUnpublishDep);
            }
          }
        }
      }
    }
  }

}
