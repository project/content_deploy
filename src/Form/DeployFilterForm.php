<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the database logging filter form.
 */
class DeployFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deploy_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'content_deploy/content_deploy.element.message';
    $params = \Drupal::request()->query->all();
    $form_state->setMethod('GET');
    $form['#prefix'] = '<div class="deploy-filter">';
    $form['#suffix'] = '</div>';
    $form['filter'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filter']['title'] = [
      '#title' => t('Title'),
      '#type' => 'search',
      '#default_value' => isset($params['title']) ? $params['title'] : '',
      '#size' => 30,
      '#maxlength' => 128
    ];
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
// If you need to display them in a drop down:
    $content_type = [];
    foreach ($node_types as $node_type) {
      $content_type[$node_type->id()] = $node_type->label();
    }
    $form['filter']['type'] = [
      '#title' => t('Content type'),
      '#type' => 'select',
      '#default_value' => isset($params['type']) ? $params['type'] : '',
      '#empty_option' => t('-Any-'),
      '#options' => $content_type
    ];

    $form['filter']['status'] = [
      '#title' => t('Published status'),
      '#type' => 'select',
      '#options' => ['1' => t('Published'), '2' => t('Unpublished') ],
      '#default_value' => isset($params['status']) ? $params['status'] : '',
      '#empty_option' => t('-Any-'),
    ];

    $langcodes = \Drupal::languageManager()->getLanguages();
    $langcodesList = [];
    if ($langcodes) {
      foreach($langcodes as $code => $lang) {
        $langcodesList[$code] = $lang->getName();
      }
    }
    $form['filter']['langcode'] = [
      '#title' => t('Language'),
      '#type' => 'select',
      '#options' => $langcodesList,
      '#empty_option' => t('-Any-'),
      '#default_value' => isset($params['langcode']) ? $params['langcode'] : '',
    ];
    // Get actual user role.
    $form['filter']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    if (!empty($params)) {
      $form['filter']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#attributes' => ['id' => 'filter-reset', 'data-target' => Url::fromRoute('content_deploy.deploy')->toString() ]
      ];
    }

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
