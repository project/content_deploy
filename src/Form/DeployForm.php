<?php

namespace Drupal\content_deploy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Provides the database logging filter form.
 */
class DeployForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deploy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $params = \Drupal::request()->query->all();

    $form['action'] = [
      '#type' => 'details',
      '#title' => $this->t('Action'),
      '#open' => TRUE,
    ];

    $envOptions = ['dev' => 'Dev', 'stage' => 'Stage', 'prod' => 'Prod'];
    $enabledEnvOptions = [];
    $defaultEnvValue = 'prod';
    foreach ($envOptions as $envValue => $env) {
      $envDetails = \Drupal::config('content_deploy.settings')->get('content_deploy.' . $envValue . '_env_url');
      if (\Drupal::currentUser()->hasPermission($envValue . ' deploy content') && !empty($envDetails)) {
        $enabledEnvOptions[$envValue] = $env;
      }
    }

    if (empty($enabledEnvOptions)) {
      $enabledEnvOptions['prod'] = 'Prod';
      $defaultEnvValue = 'prod';
    }
    elseif (!array_key_exists($defaultEnvValue, $enabledEnvOptions)) {
      $defaultEnvValue = array_key_first($enabledEnvOptions);
    }

    $form['action']['environment'] = [
      '#title' => $this->t('Target Environment'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => $enabledEnvOptions,
      '#default_value' => $defaultEnvValue,
      '#attributes' => ['class' => ['inline']],
    ];

    $form['action']['status'] = [
      '#title' => $this->t('Deployed Node Status'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => ['1' => t('Published'), '0' => t('Unpublished')],
      '#default_value' => '1',
      '#attributes' => ['class' => ['container-inline']],
    ];

    // Query for newest articles and return max 3 results.
    $query = \Drupal::entityQuery('node');
    $nodes = $query->sort('changed', 'DESC');
    if (!empty($params['title'])) {
      $query->condition('title', '%' . $params['title'] . '%', 'LIKE');
    }
    if (!empty($params['type'])) {
      $query->condition('type', $params['type'], '=');
    }
    if (!empty($params['status'])) {
      $status = ($params['status'] == 1) ? 1 : 0;
      $query->condition('status', $status);
    }
    if (!empty($params['langcode'])) {
      $query->condition('langcode', $params['langcode']);
    }
    $nodes = $query->pager(20)->execute();
    // LoadMultiple News in $nodes variable.
    $nodes = Node::loadMultiple($nodes);
    $options = [];
    if ($nodes) {
      foreach ($nodes as $node) {
        $nid = $node->id();
        $nodeUUID = $node->uuid();
        $url_options = ['absolute' => FALSE];
        $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], $url_options);
        $node_title = $node->getTitle();
        $url = $url->toString();
        $user = $node->getOwner();
        $node_link = Link::createFromRoute($node_title, 'entity.node.canonical', ['node' => $node->id()]);
        if ($user->id()) {
          $name = $user->getAccountName();
          $user_link = Link::createFromRoute($name, 'entity.user.canonical', ['user' => $user->id()]);
        }
        else {
          $user_link = t('Anonymous');
        }
        if ($node->isPublished()) {
          $status = t('Published');
        }
        else {
          $status = t('Unpublished');
        }
        $cchanged = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'custom', 'm/d/Y H:i');
        $options[$nodeUUID] = [
          'label' => ['data' => $node_link],
          'url' => ['data' => $url],
          'type' => ['data' => $node->getType()],
          'author' => ['data' => $user_link],
          'status' => ['data' => $status],
          'changed' => ['data' => $cchanged],
        ];
      }
    }
    $header = [
      'label' => [
        'data' => $this->t('Title'),
        'specifier' => 'title',
      ],
      'url' => [
        'data' => $this->t('Url'),
      ],
      'type' => [
        'data' => $this->t('Content Type'),
        'specifier' => 'type',
      ],
      'author' => [
        'data' => $this->t('Author'),
      ],
      'status' => [
        'data' => $this->t('Status'),
      ],
      'changed' => [
        'data' => $this->t('Updated'),
        'specifier' => 'changed',
        'sort' => 'desc',
      ],
    ];

    $form['action']['deploy_date_time'] = [
      '#title' => $this->t('Deployment Time'),
      '#type' => 'datetime',
      '#required' => TRUE,
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Deploy'),
    ];

    $form['result'] = [
      '#type' => 'tableselect',
      '#options'  => $options,
      '#header' => $header,
      '#empty' => $this->t('Data not found'),
      '#attributes' => ['class' => ['views-table views-view-table cols-8 responsive-enabled sticky-enabled sticky-table']],
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty($form_state->getValue('environment')) || $form_state->getValue('environment') == 'none') {
      $form_state->setErrorByName('environment', t('No Environment selected or You don\'t have access to deploy on any Environment.'));
      return;
    }

    $tableNodes = $form_state->getValue('result');
    $selectedNodes = array_filter($tableNodes, function ($singleTableNode) {
      return $singleTableNode;
    });
    if (empty($selectedNodes)) {
      $form_state->setErrorByName('result', t('No node selected for deployment. Please select the nodes.'));
      return;
    }
    else {
      foreach ($selectedNodes as $singleSelectedNode) {
        $currTimestamp = \Drupal::time()->getCurrentTime();
        $autoDeployQuery = \Drupal::database()->select('cd_auto_nodes_export', 'ane');
        $autoDeployQuery->fields('ane');
        $autoDeployQuery->condition('ane.node_uuid', $singleSelectedNode, '=');
        $autoDeployQuery->condition('ane.deployment_time', $currTimestamp, '>=');
        $autoDeployQuery->condition('ane.deployment_status', 0, '=');
        $autoDeployNodes = $autoDeployQuery->countQuery()->execute()->fetchField();
        if (!empty($autoDeployNodes) && $autoDeployNodes > 0) {
          $form_state->setErrorByName('result][' . $singleSelectedNode, t('Some selected nodes (marked red below) are already added in deployment list. Please remove them from deployment list before re-adding or you can update those deployments separately.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nodeTargetStatus = $form_state->getValue('status');

    $targetEnv = $form_state->getValue('environment');

    $deployDateTime = $form_state->getValue('deploy_date_time');
    $deployDateTime = $deployDateTime->getTimestamp();

    $current_user = User::load(\Drupal::currentUser()->id());
    $currentUserUUID = $current_user->uuid();

    $currTimestamp = \Drupal::time()->getCurrentTime();

    $tableNodes = $form_state->getValue('result');
    $selectedNodes = array_filter($tableNodes, function ($singleTableNode) {
      return $singleTableNode;
    });

    foreach ($selectedNodes as $selectedNodeUUID) {
      \Drupal::database()->insert('cd_auto_nodes_export')
        ->fields(['node_uuid', 'node_target_status', 'target_environment', 'deployment_time', 'user_uuid', 'created'])
        ->values([$selectedNodeUUID, $nodeTargetStatus, $targetEnv, $deployDateTime, $currentUserUUID, $currTimestamp])
        ->execute();
    }
    return \Drupal::messenger()->addMessage(t('Selected Nodes has been added in list of auto deployment.'), 'status');
  }

}
