<?php

namespace Drupal\content_deploy\DependencyResolver;

interface ContentSyncResolverInterface {

  public function resolve(array $normalized_entities, $visited = []);
}
