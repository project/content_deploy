<?php

namespace Drupal\content_deploy\Controller;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\content_deploy\Utility\ContentDeployNodesWithDependencyHelper;

/**
 * Import the migrated content files.
 */
class ImportMigratedContentFilesController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Import the migrated content files.
   */
  public function importFiles() {

    $importFilesParam = \Drupal::request()->query->get('import_files');

    if (!empty($importFilesParam) && $importFilesParam == 'import_migrated_files') {
      $currAutoDeployDir = \Drupal::config('content_deploy.settings')->get('content_deploy.curr_auto_deploy_dir');
      $currAutoDeployDir = rtrim($currAutoDeployDir, '/');

      if (empty($currAutoDeployDir)) {
        return new JsonResponse([
          'status' => 0,
          'successMessage' => 'Auto Deploy directory for current site is not defined.',
        ]);
      }

      // Get the files from curr_auto_deploy_dir.
      $archiveFiles = array_filter(scandir($currAutoDeployDir), function ($item) use ($currAutoDeployDir) {
        return !is_dir($currAutoDeployDir . '/' . $item) && $item != '.' && $item != '..' && substr($item, 0, 1) != '.' && strpos($item, '.tar.gz') !== FALSE;
      });

      if (!empty($archiveFiles)) {
        $importedFiles = [];
        $processedEntities = [];
        $directory = \Drupal::service('file_system')->getTempDirectory();
        $contentImporter = \Drupal::service('content_deploy.importer');

        foreach ($archiveFiles as $singleArchiveFile) {
          $path = $currAutoDeployDir . '/' . $singleArchiveFile;
          try {
            $archiver = new ArchiveTar($path, 'gz');

            $files = [];
            $attachedFiles = [];
            foreach ($archiver->listContent() as $file) {
              if (strpos($file['filename'], '/attached_files/') !== FALSE) {
                $attachedFiles[] = $file['filename'];
              }
              else {
                $files[] = $file['filename'];
              }
            }

            $archiver->extractList($attachedFiles, $directory);
            if (!empty($attachedFiles)) {
              foreach ($attachedFiles as $singleAttachedFile) {
                $fileDestinationArr = explode('attached_files', $singleAttachedFile);
                if (isset($fileDestinationArr[1]) && !empty($fileDestinationArr[1])) {
                  $fileDestinationArr = trim($fileDestinationArr[1], '/');
                  $fileDestinationArr = explode('/', $fileDestinationArr);
                  $scheme = $fileDestinationArr[0];
                  array_shift($fileDestinationArr);
                  $fileDestination = $scheme . '://' . implode('/', $fileDestinationArr);
                  $prepareDirPath = str_replace(basename($fileDestination), '', $fileDestination);
                  $prepareDir = \Drupal::service('file_system')->prepareDirectory($prepareDirPath, FileSystemInterface::CREATE_DIRECTORY);
                  if ($prepareDir) {
                    copy($directory . '/' . $singleAttachedFile, $fileDestination);
                  }
                }
              }
            }

            $archiver->extractList($files, $directory);
            if (!empty($files)) {
              foreach ($files as $file) {
                if (!file_exists($directory . '/' . $file) || in_array(basename($file), $importedFiles)) {
                  continue;
                }
                $data = file_get_contents($directory . '/' . $file);
                $ymlData = Yaml::decode($data);
                if (!in_array(basename($file), $processedEntities)) {
                  $processedEntities[] = basename($file);
                  if (isset($ymlData['_content_deploy']['entity_dependencies']) && !empty($ymlData['_content_deploy']['entity_dependencies'])) {
                    $entityDependencies = $ymlData['_content_deploy']['entity_dependencies'];
                    $helperResponse = ContentDeployNodesWithDependencyHelper::import_entity_dependencies($entityDependencies, $files, $directory, $importedFiles, $processedEntities);

                    if (!empty($helperResponse) && isset($helperResponse['importedFiles']) && !empty($helperResponse['importedFiles'])) {
                      $importedFiles = $helperResponse['importedFiles'];
                    }

                    if (!empty($helperResponse) && isset($helperResponse['processedEntities']) && !empty($helperResponse['processedEntities'])) {
                      $processedEntities = $helperResponse['processedEntities'];
                    }
                  }
                }
                $entity = $contentImporter->importEntity($ymlData);
                if ($entity) {
                  $importedFiles[] = basename($file);
                  \Drupal::logger('content_deploy')->info(t('Entity @label (@entity_type: @id) imported successfully.', [
                    '@label' => $entity->label(),
                    '@entity_type' => $entity->getEntityTypeId(),
                    '@id' => $entity->id(),
                  ]));
                }
              }
            }

            if (file_exists($path)) {
              \Drupal::service('file_system')->delete($path);
            }
            \Drupal::logger('content_deploy')->info(t('Auto Deploy import process completed successfully.'));
          }
          catch (\Exception $e) {
            \Drupal::logger('content_deploy')->error(t('Could not extract the contents of the tar file. The error message is <em>@message</em>', ['@message' => $e->getMessage()]));
          }
        }
      }

      return new JsonResponse([
        'status' => 200,
        'successMessage' => 'Migrated content files has been imported successfully.',
      ]);
    }
  }

}
