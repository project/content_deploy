<?php

namespace Drupal\content_deploy\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for content_deploy routes.
 */
class DeployController extends ControllerBase {

  /**
   *
   */
  public function deployview() {
    $build = [];
    $filterForm =  \Drupal::formBuilder()->getForm('Drupal\content_deploy\Form\DeployFilterForm');
    unset($filterForm['form_build_id']);
    unset($filterForm['form_id']);
    unset($filterForm['op']);
    $build['filter_form'] = $filterForm;
    $build['content'] = \Drupal::formBuilder()->getForm('Drupal\content_deploy\Form\DeployForm');
    return $build;

  }
}
