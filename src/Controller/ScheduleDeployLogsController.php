<?php

namespace Drupal\content_deploy\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Gives the ability to view/update/delete Schedule deployments.
 */
class ScheduleDeployLogsController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Delete deployment.
   */
  public function deleteDeployment($target_env, $node_uuid, $deployment_time, $created) {

    $autoDeployQuery = \Drupal::database()->select('cd_auto_nodes_export', 'ane');
    $autoDeployQuery->fields('ane');
    $autoDeployQuery->condition('ane.node_uuid', $node_uuid, '=');
    $autoDeployQuery->condition('ane.target_environment', $target_env, '=');
    $autoDeployQuery->condition('ane.deployment_time', $deployment_time, '=');
    $autoDeployQuery->condition('ane.created', $created, '=');
    $autoDeployNodes = $autoDeployQuery->execute()->fetchAll();

    if (!empty($autoDeployNodes)) {
      $autoDeployDltQuery = \Drupal::database()->delete('cd_auto_nodes_export');
      $autoDeployDltQuery->condition('node_target_status', $autoDeployNodes[0]->node_target_status);
      $autoDeployDltQuery->condition('target_environment', $autoDeployNodes[0]->target_environment);
      $autoDeployDltQuery->condition('deployment_time', $autoDeployNodes[0]->deployment_time);
      $autoDeployDltQuery->condition('user_uuid', $autoDeployNodes[0]->user_uuid);
      $autoDeployDltQuery->condition('created', $autoDeployNodes[0]->created);
      $autoDeployDltQuery->condition('deployment_status', $autoDeployNodes[0]->deployment_status);
      $autoDeployDltQuery->execute();
    }

    \Drupal::messenger()->addMessage(t('Deployment deleted successfully.'), 'status');
    $redirectUrl = Url::fromRoute('content_deploy.schedule_deploy_logs')->toString();
    $redirect = new RedirectResponse($redirectUrl);
    return $redirect->send();
  }

}
