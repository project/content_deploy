<?php

namespace Drupal\content_deploy\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\Component\Utility\Unicode;

/**
 * Returns responses for content bulk export.
 */
class BulkExportContentController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Downloads a tarball of the bulk exported content.
   */
  public function downloadExport() {
    $filename = 'bulk-exported-content.tar.gz';
    $file_path = \Drupal::service('file_system')->getTempDirectory() . '/' . $filename;
    if (file_exists($file_path)) {
      $mime = \Drupal::service('file.mime_type.guesser')->guess($file_path);
      $headers = [
        'Content-Type' => $mime . '; name="' . Unicode::mimeHeaderEncode(basename($file_path)) . '"',
        'Content-Length' => filesize($file_path),
        'Content-Disposition' => 'attachment; filename="' . Unicode::mimeHeaderEncode($filename) . '"',
        'Cache-Control' => 'private',
      ];
      return new BinaryFileResponse($file_path, 200, $headers);
    }
    return -1;
  }

}
