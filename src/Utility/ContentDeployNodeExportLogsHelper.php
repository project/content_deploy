<?php

namespace Drupal\content_deploy\Utility;

/**
 * Helper class for node export logs.
 */
class ContentDeployNodeExportLogsHelper {

  /**
   * Create logs for exported nodes.
   *
   * @param $nodeUUID
   *   UUID of exported node.
   * @param array $userUUID
   *   UUID of user who exported the node.
   * @param array $data
   *   Array containing data related to exported node.
   *
   * @return array
   *   Success message.
   */
  public static function createLog($nodeUUID, $userUUID = 0, $data = []) {

    if (empty($nodeUUID)) {
      \Drupal::logger('content_deploy')->error(t('Exported Node UUID cannot be empty.'));
      return [
        'errorMessage' => 'Exported Node UUID cannot be empty.',
      ];
    }

    $timestamp = \Drupal::time()->getCurrentTime();

    $serializedData = NULL;
    if (!empty($data)) {
      $serializedData = serialize($data);
    }

    \Drupal::database()->insert('cd_node_export_logs')
      ->fields(['node_uuid', 'user_uuid', 'timestamp', 'data'])
      ->values([$nodeUUID, $userUUID, $timestamp, $serializedData])
      ->execute();

    return [
      'successMessage' => 'Log maintained for exported node.',
    ];
  }

}
