<?php

namespace Drupal\content_deploy\Utility;

/**
 * Helper class for auto content deployments.
 */
class AutoContentDeployHelper {

  /**
   * Delete deployments.
   *
   * @param array $nodeUUIDs
   *   UUIDs of deployments node.
   *
   * @return
   */
  public static function deleteDeployments($nodeUUIDs) {

    if (!empty($nodeUUIDs)) {
      $autoDeployDltQuery = \Drupal::database()->delete('cd_auto_nodes_export');
      $autoDeployDltQuery->condition('node_uuid', $nodeUUIDs, 'IN');
      $autoDeployDltQuery->execute();
      return [
        'status' => 1,
      ];
    }
  }

  /**
   * Update deployments.
   *
   * @param array $nodeUUIDs
   *   UUIDs of deployments node.
   * @param string $targetEnv
   *   Target environment of deployments.
   * @param bool $targetNodeStatus
   *   Target node status of deployments.
   * @param $deploymentTime
   *   Deployment time.
   *
   * @return
   */
  public static function updateDeployments($nodeUUIDs, $targetEnv = NULL, $targetNodeStatus = NULL, $deploymentTime = NULL) {

    if (!empty($nodeUUIDs)) {

      $fields = [];

      if (!empty($targetEnv)) {
        $fields['target_environment'] = $targetEnv;
      }

      if ($targetNodeStatus !== NULL) {
        $fields['node_target_status'] = $targetNodeStatus;
      }

      if (!empty($deploymentTime)) {
        $fields['deployment_time'] = $deploymentTime;
      }

      if (!empty($fields)) {
        $currTimestamp = \Drupal::time()->getCurrentTime();
        $autoDeployUpdateQuery = \Drupal::database()->update('cd_auto_nodes_export');
        $autoDeployUpdateQuery->condition('node_uuid', $nodeUUIDs, 'IN');
        $autoDeployUpdateQuery->condition('deployment_time', $currTimestamp, '>');
        $autoDeployUpdateQuery->fields($fields);
        $autoDeployUpdateQuery->execute();
      }
      return [
        'status' => 1,
      ];
    }
  }

}
