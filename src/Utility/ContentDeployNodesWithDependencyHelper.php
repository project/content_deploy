<?php

namespace Drupal\content_deploy\Utility;

use Drupal\Component\Serialization\Yaml;

/**
 * Helper class for functionality node export/import with dependencies.
 */
class ContentDeployNodesWithDependencyHelper {

  /**
   * Exports the nodes.
   *
   * @param array $entityDependencies
   *   dependencies of exported node.
   * @param $archiver
   *   archiver of exported the node.
   * @param int $defaultOwnerUid
   *   Author UID of exported node.
   * @param array $exportedFiles
   *   array of exported files.
   *
   * @return
   */
  public static function export_entity_dependencies($entityDependencies, $archiver, $defaultOwnerUid, $exportedFiles, $nodeTargetStatus = NULL) {

    $basename = pathinfo($archiver->_tarname, PATHINFO_FILENAME);
    $exportDirName = str_replace('.' . pathinfo($basename, PATHINFO_EXTENSION), '', $basename);

    $file_system = \Drupal::service('file_system');
    $contentExporter = \Drupal::service('content_deploy.exporter');
    foreach ($entityDependencies as $entityType => $singleTypeEntityDependency) {
      foreach ($singleTypeEntityDependency as $singleEntityDependency) {
        $attachedEntityDetail = explode('.', $singleEntityDependency);
        $attachedEntityUUID = end($attachedEntityDetail);
        if (!empty($attachedEntityUUID)) {
          if (in_array($entityType . '.' . $attachedEntityUUID, $exportedFiles)) {
            continue;
          }
          $attachedEntity = \Drupal::service('entity.repository')->loadEntityByUuid($entityType, $attachedEntityUUID);
          if ($attachedEntity->hasField('uid') && empty($attachedEntity->get('uid')->value)) {
            $attachedEntity->set('uid', $defaultOwnerUid);
          }
          if (!empty($attachedEntity)) {

            if ($entityType == 'node' && $nodeTargetStatus !== NULL && $nodeTargetStatus === 0) {
              $attachedEntity->setUnpublished();
            }
            elseif ($entityType == 'node' && $nodeTargetStatus === 1) {
              $attachedEntity->setPublished();
            }

            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            if (!empty($language) && $attachedEntity->hasTranslation($language)) {
              $attachedEntity = $attachedEntity->getTranslation($language);
            }
            $attachedEntityBundle = $attachedEntity->bundle();
            $attachedEntityFileName = $entityType . "." . $attachedEntityBundle . "." . $attachedEntityUUID;
            $attachedExportedEntity = $contentExporter->exportEntity($attachedEntity, []);
            $attachedDecodedEntity = Yaml::decode($attachedExportedEntity);
            if ($entityType == 'file') {
              $uri = $attachedEntity->getFileUri();
              if (file_exists($uri)) {
                // $scheme = $file_system->uriScheme($uri);
                $scheme = \Drupal::service('stream_wrapper_manager')->getScheme($uri);
                $archiver->addModify([$uri], "$exportDirName/attached_files/$scheme", "$scheme://");
              }
            }
            $archiver->addString("$exportDirName/entities/$entityType/$attachedEntityBundle/$attachedEntityFileName.yml", $attachedExportedEntity);
            $exportedFiles[] = $entityType . '.' . $attachedEntityUUID;
            if (isset($attachedDecodedEntity['_content_deploy']['entity_dependencies']) && !empty($attachedDecodedEntity['_content_deploy']['entity_dependencies'])) {
              /* Export entity_dependencies recursively. */
              self::export_entity_dependencies($attachedDecodedEntity['_content_deploy']['entity_dependencies'], $archiver, $defaultOwnerUid, $exportedFiles, $nodeTargetStatus);
            }
          }
        }
      }
    }
    return [
      'exportedFiles' => $exportedFiles,
    ];
  }

  /**
   * Imports the nodes.
   *
   * @param array $entityDependencies
   *   Dependencies of node being imported.
   * @param array $files
   *   Files of extracted archiver.
   * @param $directory
   *   Directory where archiver got extracted.
   * @param array $importedFiles
   *   array of imported files.
   * @param array $processedEntities
   *   array of processed files.
   *
   * @return
   */
  public static function import_entity_dependencies($entityDependencies, $files, $directory, $importedFiles = [], $processedEntities = []) {

    if (empty($entityDependencies) || empty($files) || empty($directory)) {
      \Drupal::logger('content_deploy')->error(t('Entity dependencies or content files or directory name is empty'));
      return;
    }

    $contentImporter = \Drupal::service('content_deploy.importer');

    foreach ($entityDependencies as $entityType => $singleTypeEntityDependency) {
      foreach ($singleTypeEntityDependency as $singleEntityDependency) {
        $singleEntityDependencyFile = $singleEntityDependency . '.yml';
        $matches = array_filter($files, function ($var) use ($singleEntityDependencyFile) {
          return preg_match("/\b$singleEntityDependencyFile\b/i", $var);
        });

        if (!empty($matches)) {
          $file = reset($matches);
          if (!file_exists($directory . '/' . $file) || in_array(basename($file), $importedFiles)) {
            continue;
          }
          $data = file_get_contents($directory . '/' . $file);
          $ymlData = Yaml::decode($data);
          if (!in_array(basename($file), $processedEntities)) {
            $processedEntities[] = basename($file);
            if (isset($ymlData['_content_deploy']['entity_dependencies']) && !empty($ymlData['_content_deploy']['entity_dependencies'])) {
              $entityDependencies = $ymlData['_content_deploy']['entity_dependencies'];
              self::import_entity_dependencies($entityDependencies, $files, $directory, $importedFiles, $processedEntities);
            }
          }
          $entity = $contentImporter->importEntity($ymlData);
          if ($entity) {
            $importedFiles[] = basename($file);
            \Drupal::logger('content_deploy')->info(t('Entity @label (@entity_type: @id) imported successfully.', [
              '@label' => $entity->label(),
              '@entity_type' => $entity->getEntityTypeId(),
              '@id' => $entity->id(),
            ]));
          }
        }
      }
    }

    return [
      'importedFiles' => $importedFiles,
      'processedEntities' => $processedEntities,
    ];
  }

}
