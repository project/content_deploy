<?php

namespace Drupal\content_deploy\Plugin\Action;

use Drupal\Component\Serialization\Yaml;
use Drupal\content_deploy\Utility\ContentDeployNodeExportLogsHelper;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides an Archive Node Action.
 *
 * @Action(
 *   id = "export_node_action",
 *   label = @Translation("Export Content"),
 *   type = "node",
 *   category = @Translation("Custom")
 * )
 */
class ExportNodeAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * @var
   */
  protected $exportedFiles = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function access($node, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $node */
    // Cn be do add the logic to access this action.
    $access = $node->access('update', $account, TRUE)
      ->andIf($node->title->access('edit', $account, TRUE));
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $file_system = \Drupal::service('file_system');
    $contentExporter = \Drupal::service('content_deploy.exporter');
    $archiveFilepath = \Drupal::service('file_system')->getTempDirectory() . '/bulk-exported-content.tar.gz';
    if (file_exists($archiveFilepath)) {
      $file_system->delete($archiveFilepath);
    }
    $archiver = new ArchiveTar($archiveFilepath, 'gz');
    foreach ($entities as $entity) {
      $entity_type = $entity->getEntityTypeId();
      $entity_bundle = $entity->bundle();

      // Generate the YAML file.
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($entity->hasTranslation($language)) {
        $entity = $entity->getTranslation($language);
      }
      $defaultOwnerUid = 0;
      $defaultOwnerUUID = 0;
      if ($entity->getOwnerId()) {
        $defaultOwnerUid = $entity->getOwnerId();
        $defaultOwnerUUID = $entity->getOwner()->uuid();
      }
      $exported_entity = $contentExporter->exportEntity($entity, []);

      // Create the name.
      $nodeFileName = $entity_type . "." . $entity_bundle . "." . $entity->uuid();

      /* get the node dependencies */
      if (!empty($exported_entity)) {
        $decodedEntity = Yaml::decode($exported_entity);
        if (isset($decodedEntity['_content_deploy']['entity_dependencies']) && !empty($decodedEntity['_content_deploy']['entity_dependencies'])) {
          $entityDependencies = $decodedEntity['_content_deploy']['entity_dependencies'];
          /* Export entity_dependencies recursively. */
          $this->_export_entity_dependencies($entityDependencies, $archiver, $defaultOwnerUid);
        }
        /* get the parent node */
        $this->exportedFiles[] = $entity->getEntityTypeId() . '.' . $entity->uuid();
        ContentDeployNodeExportLogsHelper::createLog($entity->uuid(), $defaultOwnerUUID);
        $archiver->addString("bulk-exported-content/entities/$entity_type/$entity_bundle/$nodeFileName.yml", $exported_entity);
      }
    }

    return \Drupal::messenger()->addStatus(t('The content was exported successfully. <a href=":export-download">Download tar file</a>', [':export-download' => Url::fromRoute('content.bulk_export_download')->toString()]));
  }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    $this->executeMultiple([$object]);
  }

  /**
   * Provides help to export child dependencies.
   */
  private function _export_entity_dependencies($entityDependencies, $archiver, $defaultOwnerUid) {
    $file_system = \Drupal::service('file_system');
    $contentExporter = \Drupal::service('content_deploy.exporter');
    foreach ($entityDependencies as $entityType => $singleTypeEntityDependency) {
      foreach ($singleTypeEntityDependency as $singleEntityDependency) {
        $attachedEntityDetail = explode('.', $singleEntityDependency);
        $attachedEntityUUID = end($attachedEntityDetail);
        if (!empty($attachedEntityUUID)) {
          if (in_array($entityType . '.' . $attachedEntityUUID, $this->exportedFiles)) {
            continue;
          }
          $attachedEntity = \Drupal::service('entity.repository')->loadEntityByUuid($entityType, $attachedEntityUUID);
          if ($attachedEntity->hasField('uid') && empty($attachedEntity->get('uid')->value)) {
            $attachedEntity->set('uid', $defaultOwnerUid);
          }
          if (!empty($attachedEntity)) {
            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            if (!empty($language) && $attachedEntity->hasTranslation($language)) {
              $attachedEntity = $attachedEntity->getTranslation($language);
            }
            $attachedEntityBundle = $attachedEntity->bundle();
            $attachedEntityFileName = $entityType . "." . $attachedEntityBundle . "." . $attachedEntityUUID;
            $attachedExportedEntity = $contentExporter->exportEntity($attachedEntity, []);
            $attachedDecodedEntity = Yaml::decode($attachedExportedEntity);
            if ($entityType == 'file') {
              $uri = $attachedEntity->getFileUri();
              if (file_exists($uri)) {
                // $scheme = $file_system->uriScheme($uri);
                $scheme = \Drupal::service('stream_wrapper_manager')->getScheme($uri);
                $archiver->addModify([$uri], "bulk-exported-content/attached_files/$scheme", "$scheme://");
              }
            }
            $archiver->addString("bulk-exported-content/entities/$entityType/$attachedEntityBundle/$attachedEntityFileName.yml", $attachedExportedEntity);
            $this->exportedFiles[] = $entityType . '.' . $attachedEntityUUID;
            if (isset($attachedDecodedEntity['_content_deploy']['entity_dependencies']) && !empty($attachedDecodedEntity['_content_deploy']['entity_dependencies'])) {
              /* Export entity_dependencies recursively. */
              $this->_export_entity_dependencies($attachedDecodedEntity['_content_deploy']['entity_dependencies'], $archiver, $defaultOwnerUid);
            }
          }
        }
      }
    }
  }

}
